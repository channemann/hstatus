from datetime import datetime
import json
import sys
import os

doses = json.loads(open('set_dose.json').read())
last_temp = doses[0]

last_temp_time = datetime.fromtimestamp(os.stat('set_dose.json').st_mtime)

# if not (last_temp.get('recieved') == True):
#    sys.exit(0)

# "2015-11-20T15:15:02.643157"
# last_temp_timestamp = last_temp['timestamp']
# try:
#     last_temp_time = datetime.strptime(last_temp_timestamp, '%Y-%m-%dT%H:%M:%S.%f')
# except ValueError:
#     last_temp_time = datetime.strptime(last_temp_timestamp, '%Y-%m-%dT%H:%M:%S')

# lexicographically compare last temp basal time with "HH:MM:SS" times from the basal profile
basals = json.loads(open('read_selected_basal_profile.json').read())
current_basal = None
for basal, next_basal in zip(basals[:-1], basals[1:]):
    if basal['start'] <= last_temp_time.strftime('%H:%M:%S') < next_basal['start']:
        current_basal = basal
        break
if current_basal is None:
    current_basal = basals[-1]

net_basal = last_temp['rate'] - current_basal['rate']

formatted_time = '{:%I:%M%P}'.format(last_temp_time).replace('m', '').lower()
if formatted_time[0] == '0':
    formatted_time = formatted_time[1:]

iob_history = json.loads(open('iob_history.json').read())
iob = [iob for iob in iob_history if iob['date'] < datetime.now().strftime("%Y-%m-%dT%H:%M:%S")][-1]['amount']

out = '{:+.2f} u/hr @ {}\nIOB = {:.2f} u'.format(net_basal, formatted_time, iob)

with open(sys.argv[1], 'w') as f:
    f.write(out)
